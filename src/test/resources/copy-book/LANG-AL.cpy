               01  LANG-AL.
                   15  LANG-AL-KEY.
                       20  LANG-AL-TYPE       PIC XX.
                       20  LANG-AL-CODE       PIC X(2).
                       20                     PIC X(16).
                   15  LANG-AL-TITLE-38-ASSIGNMENT.
                       20  LANG-AL-DESC-1     PIC X(20).
                       20                     PIC X.
                       20  LANG-AL-DESC-2     PIC X(40).
                   15  LANG-AL-SCHED          PIC X(4).
                   15  LANG-AL-ASSIGN-IND     PIC X.
                   15                         PIC X(89).
                   15  LANG-AL-EFFECTIVE-DATE PIC 9(5).
                   15  FILLER                 PIC X(20).