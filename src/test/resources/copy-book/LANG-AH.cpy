               01  LANG-AH.                                                 
                   15  LANG-AH-KEY.
                       20  LANG-AH-TYPE                  PIC XX.            
                       20  LANG-AH-RESIDENT-SCHED        PIC X(4).          
                       20  FILLER                        PIC X(14).         
                   15  LANG-AH-STIPENDS.
                       20  LANG-AH-STEP-1                PIC 9(7).
                       20  LANG-AH-STEP-2                PIC 9(7).
                       20  LANG-AH-STEP-3                PIC 9(7).
                       20  LANG-AH-STEP-4                PIC 9(7).
                       20  LANG-AH-STEP-5                PIC 9(7).
                       20  LANG-AH-STEP-6                PIC 9(7).
                       20  LANG-AH-STEP-7                PIC 9(7).
                       20  LANG-AH-STEP-8                PIC 9(7).
                   15  REDEFINES LANG-AH-STIPENDS.
                       20  LANG-AH-STEP  OCCURS 8 TIMES  PIC 9(7).
                   15  FILLER                            PIC X(99).
                   15  LANG-AH-EFFECTIVE-DATE            PIC 9(5).
                   15  FILLER                            PIC X(20).