               10  LANG-UA-LN1-REC.
                       20  LANG-UA-LN1-TYPE           PIC XX.
                       20  LANG-UA-LN1-UNION-CODE     PIC X(3).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-INACTIVE       PIC X.
                       20                             PIC X(11).
                       20  LANG-UA-LN1-LINE           PIC X.
                       20  LANG-UA-LN1-NAME           PIC X(35).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-ADDR1          PIC X(35).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-ADDR2          PIC X(35).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-CITY           PIC X(18).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-ST             PIC X(02).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-ZIP            PIC X(05).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-AFGE-PAYMT     PIC X(01).
                       20                             PIC X(2).
                       20  LANG-UA-LN1-SUPPRESS       PIC X(01).
                       20                             PIC X(09).
                       20  FILLER                     PIC X(25).
                       20  LANG-UA-LN2-TYPE           PIC XX.
                       20  LANG-UA-LN2-UNION-CODE     PIC X(3).
                       20                             PIC X(2).
                       20  LANG-UA-LN2-INACTIVE       PIC X.
                       20                             PIC X.
                       20                             PIC X(10).
                       20  LANG-UA-LN2-LINE           PIC X.
                       20                             PIC X(30).
                       20  LANG-UA-LN2-AFGE-PAYMT     PIC X(01).
                       20                             PIC X(2).
                       20  LANG-UA-LN2-SUPPRESS       PIC X(01).
                       20                             PIC X(2).
                       20  LANG-UA-LN2-EMAIL-ADDR1    PIC X(40).
                       20                             PIC X(2).
                       20  LANG-UA-LN2-EMAIL-ADDR2    PIC X(40).
                       20                             PIC X(37).
                       20  FILLER                     PIC X(25).
                       20  LANG-UA-LN3-TYPE           PIC XX.
                       20  LANG-UA-LN3-UNION-CODE     PIC X(3).
                       20                             PIC XX.
                       20  LANG-UA-LN3-INACTIVE       PIC X.
                       20                             PIC X.
                       20  LANG-UA-LN3-UNION-OPT      PIC X.
                       20                             PIC X.
                       20  LANG-UA-LN3-EFF-DATE.      PIC X(08).
                       20                             PIC X.
                       20  LANG-UA-LN3-AMOUNT         PIC 9(05).
                       20                             PIC X(150).
                       20                             PIC X(25).