              01 LANG-PS.
                    20 LANG-PS-TYPE            PIC XX.                  
                    20 LANG-PS-NURSE-STA       PIC XXX.                 
                    20 LANG-PS-DUTY-STA        PIC XX.                  
                    20 LANG-PS-NURSE-SCHED     PIC X(4).                
                    20 FILLER                  PIC X(09).               
                 20 LANG-PS-NURSE-DESC         PIC X(60).               
                 20 FILLER                     PIC X(95).
                 20  LANG-PS-EFFECTIVE-DATE    PIC 9(5).
                 20  FILLER                    PIC X(20).