              01 LANG-ED.
                    20 LANG-ED-TYPE            PIC XX.
                    20 LANG-ED-POI             PIC X(4).
                    20                         PIC X(3).
                    20 LANG-ED-INACTIVE        PIC X.
                    20                         PIC X(10).
                    20 LANG-ED-FIRST-NAME      PIC X(30).
                    20                         PIC X.
                    20 LANG-ED-MIDDLE-INIT     PIC X.
                    20                         PIC X.
                    20 LANG-ED-LAST-NAME       PIC X(30).
                    20                         PIC X.
                    20 LANG-ED-SUFFIX          PIC XXX.
                    20                         PIC X.
                    20 LANG-ED-TITLE           PIC X(38).
                    20                         PIC X(49).
                 20  LANG-ED-EFFECTIVE-DATE    PIC 9(5).
                 20  FILLER                    PIC X(20).