               01  LANG-EE-REC.
                       20  LANG-EE-LN1-TYPE           PIC XX.
                       20  LANG-EE-LN1-POI            PIC X(4).
                       20                             PIC X.
                       20  LANG-EE-LN1-LINE           PIC X.
                       20                             PIC X.
                       20  LANG-EE-LN1-INACTIVE       PIC X.
                       20                             PIC X(10).
                       20  LANG-EE-LN1-EMPLOYER-NAME  PIC X(45).
                       20                             PIC X(5).
                       20  LANG-EE-LN1-ADDR-LINE-1    PIC X(40).
                       20                             PIC X(5).
                       20  LANG-EE-LN1-ADDR-LINE-2    PIC X(40).
                       20                             PIC X(20).
                   20  FILLER                         PIC X(25).
                       20  LANG-EE-LN2-ADDR-LINE-3    PIC X(40).
                       20                             PIC X(5).
                       20  LANG-EE-LN2-CITY           PIC X(25).
                       20                             PIC X(5).
                       20  LANG-EE-LN2-STATE          PIC X(2).
                       20                             PIC X(5).
                       20  LANG-EE-LN2-ZIP1       PIC X(5).
                       20                         PIC X.
                       20  LANG-EE-LN2-ZIP2       PIC X(4).
                       20                             PIC X(1).
                       20  LANG-EE-LN2-STANO          PIC X(3).
                       20                             PIC X(1).
                       20  LANG-EE-LN2-DUTYSTA        PIC X(2).
                       20                             PIC X(56).
                   20  LANG-EE-LN2-EFFECTIVE-DATE     PIC 9(5).
                   20  FILLER                         PIC X(20).