               01  LANG-EB.
                       20  LANG-EB-TYPE                  PIC XX.
                       20  LANG-EB-STATION           PIC XXX.
                       20  LANG-EB-DTY-STATION       PIC XX.
                       20                                PIC XX.
                       20  LANG-EB-ACTIVE                PIC X.
                       20                                PIC X(10).
                       20  LANG-EB-PAYROLL-OFFICE-NO    PIC X(4).
                       20                               PIC X.
                       20  LANG-EB-ENLS-W2-STATION      PIC XXX.
                       20                               PIC X.
                       20  LANG-EB-HR-STATION           PIC XXX.
                       20                               PIC X.
                       20  LANG-EB-PR-STATION           PIC XXX.
                       20                               PIC X.
                       20  LANG-EB-NO-SRC               PIC X.
                       20                               PIC X.
                       20  LANG-EB-ELIG-AVAIL           PIC X.
                       20                               PIC X.
                       20  LANG-EB-COLA                 PIC 9(5).
                       20                               PIC X.
                       20  LANG-EB-EOPF                 PIC X.
                       20                               PIC X.
                       20  LANG-EB-POST-DIFF            PIC 9(5).
                       20                               PIC X.
                       20  LANG-EB-STOP-EXAM            PIC X.
                       20                               PIC X(62).
                       20  LANG-EB-WAGE-SURVEY          PIC XXX.
                       20  LANG-EB-VCS-SURVEY           PIC X(3).
                       20                               PIC X(51).
                       20  LANG-EB-EFFECTIVE-DATE       PIC 9(5).
                       20  FILLER                       PIC X(20).