               01  LANG-AC.
                   15  LANG-AC-KEY.
                       20  LANG-AC-K-ID                PIC XX.
                       20  LANG-AC-OCCUPATION.
                           25  LANG-AC-OCCUP-SERIES    PIC X(4).
                           25  LANG-AC-TITLE-CODE      PIC XX.
                       20  LANG-AC-ASSIGNMENT          PIC XX.
                       20                              PIC X(10).
                   15  LANG-AC-DATA.
                       20  LANG-AC-OCCUP-DESCRIPTION1  PIC X(39).
                       20                              PIC X.
                       20  LANG-AC-POSITION-TITLE      PIC X(25).
                       20                              PIC X.
                       20  LANG-AC-PREM-PAY-IND        PIC X.
                       20  LANG-AC-WKEND-PREM-PAY-IND  PIC X.
                       20  LANG-AC-SUBACCOUNT          PIC XX.
                       20                              PIC X.
                       20  LANG-AC-FTEE-ADJUST-FACTOR  PIC X.
                       20                              PIC X.
                       20  LANG-AC-PARENTICAL-ABBR     PIC XX.
                       20                              PIC X.
                       20  LANG-AC-PPLAN-X-OCC-SER     PIC X.
                       20                              PIC X.
                       20  LANG-AC-HYBRID              PIC X.
                       20                              PIC X.
                       20  LANG-AC-MIN-GRADE           PIC XX.
                       20                              PIC X.
                       20  LANG-AC-MAX-GRADE           PIC XX.
                       20                              PIC X.
                       20  LANG-AC-PICK-CH-IND         PIC X.
                       20                              PIC X(13).
                       20  LANG-AC-UNIF-ALLOW-HR-RATE  PIC X(5).
                       20                              PIC X.
                       20  LANG-AC-UA-PER-ANNUM        PIC X(5).
                       20                              PIC X(44).
                   15 LANG-AC-EFFECTIVE-DATE           PIC X(5).
                   15 FILLER                           PIC X(20).