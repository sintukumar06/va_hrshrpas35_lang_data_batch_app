              01 LANG-RS.
                    20 LANG-RS-TYPE            PIC XX.
                    20 LANG-RS-STA             PIC XXX.
                    20 LANG-RS-DUTY            PIC XX.
                    20 LANG-RS-SCHED           PIC X(4).
                    20                         PIC X(9).
                    20 LANG-RS-SCHED-DESC      PIC X(40).
                 20                            PIC X(115).
                 20  LANG-RS-EFFECTIVE-DATE    PIC 9(5).
                 20  FILLER                    PIC X(20).