              01 LANG-AM.
                 15 LANG-AM-TYPE          PIC XX.
                 15 LANG-AM-DMS-CC-CODE   PIC X(8).
                 15                       PIC X(10).
                 15 LANG-AM-DMS-CC-DESC-1    PIC X(25).
                 15                          PIC X.
                 15 LANG-AM-OBLIG-NBR        PIC X(6).
                 15                          PIC X.
                 15 LANG-AM-FUND-CTRL-PT     PIC X(3).
                 15                          PIC X.
                 15 LANG-AM-LD-CODE          PIC X(4).
                 15                          PIC X.
                 15 LANG-AM-FMS-FUND-CODE    PIC X(6).
                 15                          PIC X(107).
                 15                          PIC 9(5).
                 15  FILLER                  PIC X(20).