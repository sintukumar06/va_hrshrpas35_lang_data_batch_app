              01 LANG-15.
                 15 LANG-15-KEY.
                    20 LANG-15-TYPE            PIC XX.
                    20 LANG-15-STA             PIC XXX.
                    20 LANG-15-DUTY            PIC XX.
                    20 LANG-15-OPM             PIC X(4).
                    20                         PIC X(9).
                 15 LANG-15-DATA.
                    20 LANG-15-OPM-DESC        PIC X(40).
                 15                            PIC X(115).
                 15 LANG-15-EFFECTIVE-DATE     PIC X(5).
                 15 FILLER                     PIC X(20).