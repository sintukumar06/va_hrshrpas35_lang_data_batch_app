               01  LANG-BL.
                       20  LANG-BL-TYPE            PIC XX.
                       20  LANG-BL-WAGE-AREA       PIC XXX.
                       20  LANG-BL-PAY-PLAN        PIC XX.
                       20  LANG-BL-PAY-GRADE       PIC XX.
                       20                          PIC X(11).
                    20 FILLER                  PIC X(3).
                    20 LANG-BL-PAYRATE-1       PIC 99V99.
                    20 FILLER                  PIC X(3).
                    20 LANG-BL-PAYRATE-2       PIC 99V99.
                    20 FILLER                  PIC X(3).
                    20 LANG-BL-PAYRATE-3       PIC 99V99.
                    20 FILLER                  PIC X(3).
                    20 LANG-BL-PAYRATE-4       PIC 99V99.
                    20 FILLER                  PIC X(3).
                    20 LANG-BL-PAYRATE-5       PIC 99V99.
                    20 FILLER                  PIC X(120).
                   20  LANG-BL-EFFECTIVE-DATE      PIC 9(5).
                   20  FILLER                      PIC X(20).