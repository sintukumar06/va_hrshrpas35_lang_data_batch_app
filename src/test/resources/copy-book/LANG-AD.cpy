               01  LANG-AD.
                   15  LANG-AD-KEY.
                       20  LANG-AD-TYPE               PIC XX.
                       20  LANG-AD-CC-CODE            PIC X(8).
                       20                             PIC X(10).
                   15  LANG-AD-ORGANIZATN-COST-CENTER.
                       20  LANG-AD-OCC-DESC-1         PIC X(44).
                       20                             PIC X.
                       20  LANG-AD-OCC-DESC-2         PIC X(25).
                   15                                 PIC X(14).
                   15  LANG-AD-FFUND-CC               PIC X.
                   15                                 PIC X.
                   15  LANG-AD-DLD-CODE               PIC X(4).
                   15                                 PIC X.
                   15  LANG-AD-FMS-FUND               PIC X(6).
                   15                                 PIC X(58).
                   15 LANG-AD-EFFECTIVE-DATE          PIC X(5).
                   15 FILLER                          PIC X(20).