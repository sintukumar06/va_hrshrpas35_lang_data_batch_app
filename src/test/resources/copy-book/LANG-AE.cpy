               01  LANG-AE.
                       20  LANG-AE-K-ID            PIC XX.
                       20  LANG-AE-K-STA-NUM       PIC XXX.
                       20  LANG-AE-K-DUTY-STA      PIC XX.
                       20                          PIC XX.
                       20  LANG-AE-K-STA-INACT     PIC X.
                       20                          PIC X(10).
                       20  LANG-AE-TYPE-ACT    PIC X(5).
                       20  LANG-AE-CITY    PIC X(14).
                       20  LANG-AE-STATE   PIC XX.
                       20  LANG-AE-SON             PIC X(4).
                       20  LANG-AE-GSA-STATE   PIC XX.
                       20  LANG-AE-GSA-CITY    PIC X(4).
                       20  LANG-AE-GSA-COUNTY  PIC XXX.
                       20  LANG-AE-VBA-AREA        PIC X.
                       20  LANG-AE-MSA-CODE        PIC X(4).
                       20  LANG-AE-ACT-NAME        PIC X(21).
                       20  LANG-AE-ZIP-5       PIC X(5).
                       20  LANG-AE-ZIP-4       PIC X(4).
                       20  LANG-AE-MED-DIST        PIC XX.
                       20  LANG-AE-ARS-XMIT        PIC X.
                       20  LANG-AE-ARS-CODE        PIC X(7).
                       20  LANG-AE-DHCP-IND        PIC X.
                       20  LANG-AE-FMS-CODE        PIC XX.
                       20  LANG-AE-PRIOR-GAP       PIC X.
                       20  LANG-AE-SVC-STA         PIC XXX.
                       20  LANG-AE-VHA-REGION      PIC X.
                       20  LANG-AE-STATE-TX-CODE   PIC XX.
                       20  LANG-AE-CITY-TX-CODE    PIC X(6).
                       20  LANG-AE-VCS-DIST        PIC X.
                       20  LANG-AE-NCS-DIST        PIC X.
                       20  LANG-AE-LOC-PAY         PIC X(4).
                       20  LANG-AE-DFAS-STA        PIC X.
                       20  LANG-AE-DFAS-CONV-DATE  PIC X(8).
                       20  LANG-AE-VBA-AREA-PAID   PIC XX.
                       20  LANG-AE-VHA-REGION-PAID PIC XX.
                       20  LANG-AE-VCS-DIST-PAID   PIC XX.
                       20  LANG-AE-NCS-DIST-PAID   PIC XX.
                       20  LANG-AE-HR-STA          PIC X.
                       20  LANG-AE-HR-CONV-DATE    PIC X(8).
                       20                           PIC X(28).
                       20  LANG-AE-EFFECTIVE-DATE   PIC 9(5).
                       20  FILLER                   PIC X(20).