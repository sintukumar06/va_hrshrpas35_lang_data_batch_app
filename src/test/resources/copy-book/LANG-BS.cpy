               01  LANG-BS.
                       20  LANG-BS-TYPE          PIC XX.
                       20  LANG-BS-OPM           PIC X(4).
                       20  LANG-BS-GRADE         PIC XX.
                       20                        PIC X(12).
                       20                            PIC X.
                       20  LANG-BS-MANUAL            PIC X.
                       20                            PIC X.
                       20  LANG-BS-STEP1-RATE        PIC 9(7).
                       20                            PIC X.
                       20  LANG-BS-STEP-INCREMENT    PIC 9(5).
                       20                            PIC X.
                       20  LANG-BS-ABOVE-MINIMUM     PIC XX.
                       20                            PIC X.
                       20  LANG-BS-EFFECTIVE-DATE    PIC 9(7).
                       20  LANG-BS-SALARY-1          PIC 9(7).
                       20  LANG-BS-SALARY-2          PIC 9(7).
                       20  LANG-BS-SALARY-3          PIC 9(7).
                       20  LANG-BS-SALARY-4          PIC 9(7).
                       20  LANG-BS-SALARY-5          PIC 9(7).
                       20  LANG-BS-SALARY-6          PIC 9(7).
                       20  LANG-BS-SALARY-7          PIC 9(7).
                       20  LANG-BS-SALARY-8          PIC 9(7).
                       20  LANG-BS-SALARY-9          PIC 9(7).
                       20  LANG-BS-SALARY-10         PIC 9(7).
                       20                            PIC X.
                       20  LANG-BS-SUPPLEMENT        PIC 9(5).
                       20                            PIC X.
                       20  LANG-BS-PA-EFDA           PIC X.
                       20                            PIC X(50).
                   20  LANG-BS-EFFECTIVE-DATE        PIC 9(5).
                   20  FILLER                        PIC X(20).